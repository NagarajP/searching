package com.myzee.searching;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CollectionsDotBinarySearch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> list = new ArrayList<Integer>();
		list.add(14);
		list.add(1);
		list.add(88);
		list.add(3);
		int key = 3;
		
		Collections.sort(list);
		Iterator<Integer> itr = list.iterator();
		while (itr.hasNext()) {
			Integer integer = (Integer) itr.next();
			System.out.println(integer);
			
		}
		
		int searchIndex = Collections.binarySearch(list, key);
		System.out.println(key + " found at " + searchIndex);
		
		
	}

}
