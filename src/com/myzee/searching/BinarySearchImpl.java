package com.myzee.searching;

import java.util.Arrays;

public class BinarySearchImpl {

	public static void main(String[] args) {
		int a[] = { 1, 6, 8, 16, 57, 89, 600 };
		int key = 890;
		search(a, key);
	}

	private static void search(int[] a, int key) {
		int len = a.length - 1;
		int mid = len / 2;
		if (len != 0) {
			if (a[mid] == key) {
				System.out.println("found");
			} else if (key < a[mid]) {
				search(Arrays.copyOfRange(a, 0, mid), key);
			} else {
				search(Arrays.copyOfRange(a, mid + 1, a.length), key);
			}
		} else {
			System.out.println("not found");
		}
	}

}
