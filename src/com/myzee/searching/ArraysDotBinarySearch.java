package com.myzee.searching;

import java.util.Arrays;

public class ArraysDotBinarySearch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] arrayInt = {12, 4, 65, 17, 98, 43};
		
		int[] intArr = new int[5];
		intArr[0] = 10;
		intArr[1] = 2;
		intArr[2] = 45;
		intArr[3] = 3;
		intArr[4] = 66;
		
		int key = 10;
		Arrays.sort(intArr);
		for (int i : intArr) {
			System.out.println(i);
		}
		int searchIndex = Arrays.binarySearch(intArr, key);
		System.out.println(key + " found at " + searchIndex);
		
		System.out.println();
		
		key = 12;
		Arrays.sort(arrayInt);
		for(int j : arrayInt) {
			System.out.println(j);
		}
		searchIndex = Arrays.binarySearch(arrayInt, key);
		System.out.println(key + " found at " + searchIndex);
		
		System.out.println("\nSearching string inside the array of strings\n");
		//Search String in array of strings
		String[] strArr = {"abc", "acb", "bca", "bac"};
		for(String s : strArr) {
			System.out.println(s);
		}
		System.out.println("\nAfter Sorting the string array\n");
		Arrays.sort(strArr);
		for(String s : strArr) {
			System.out.println(s);
		}
		
		searchIndex = Arrays.binarySearch(strArr, "bac");
		System.out.println("string found at " + searchIndex + " location");
		
	}

}
