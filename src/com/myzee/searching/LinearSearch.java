package com.myzee.searching;


/**
 * 
 * @author Nagaraj
 * Description : In linear search best case for time complexity will be O(1) and worst case will be O(n). 
 * 
 */
public class LinearSearch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int searchKey = 9;
		int[] intArr = {15, 33, 8, 9, 47};
		int pos = LinearSrch(intArr, searchKey);
		System.out.println(searchKey + " found at " + pos);
		
		String[] strArr = {"bca", "abc", "cba"};
		String key = "cba";
		pos = LinearSrchForString(strArr, key);
		System.out.println(key + " found at " + pos);
	}
	
	private static int LinearSrch(int[] intArr, int key) {
		for (int i = 0; i < intArr.length; i++)
			if(key == intArr[i])
				return i;
		return -1;
	}
	
	private static int LinearSrchForString(String[] strArr, String key) {

		for (int i = 0; i < strArr.length; i++) {
			if(key.equals(strArr[i])) {
				return i;
			}
		}
		return -1;
	}
}
